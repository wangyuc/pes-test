import numpy as np
import matplotlib.pyplot as plt

def f(x):
    return x**2

gridl = 10

x = np.linspace(-gridl, gridl, 1000)
y = f(x) #Broadcasting

fig, ax = plt.subplots()

ax.plot(y, label="Quadratic potential")
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.legend()

plt.show()