

class person():
    """
    Documentation comment
    """
    def __init__(self, name, age):
        self.name = name
        self.age = age

class employee(person):
    def __init__(self, name, age, salary):
        super().__init__(name, age)
        self.salary = salary


#inheritance

john = person(name='John', age=39)

print(john.age)


butcher_john = employee(name='john', age=39, salary=1000)

print(butcher_john.salary)
print(butcher_john.age)