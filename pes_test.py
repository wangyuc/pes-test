import numpy as np
import matplotlib.pyplot as plt
from polylib.PES import _base

class anharmonic(_base.BasePES):
    """
    Anharmonic potential interpolates harmonic and Morse potential.
    """
    def __init__(self, x0, a0, d0, lamb, e0=0.0):
        self.x0 = x0
        self.a0 = a0
        self.d0 = d0
        self.lamb = lamb
        self.e0 = e0
        self.mass = 1

    def potential(self, x):
        return (1-self.lamb)*self.d0*self.a0**2*(x-self.x0)**2 + \
                self.lamb*self.d0*(1-np.exp(-self.a0*(x-self.x0)))**2 + self.e0


lamb = 1
d0 = 10.0
a0 = 1.0
v1 = anharmonic(x0=-1.0, a0=a0, d0=d0, lamb=lamb)
v2 = anharmonic(x0=1.0, a0=-a0, d0=d0, lamb=lamb)


###### plot PES

x = np.linspace(-3,3,100)
y1 = v1.potential(x)
y2 = v2.potential(x)

fig, (ax1) = plt.subplots(1, 1, figsize=(10,10))

ax1.plot(x, y1, color='red', label="V1")
ax1.plot(x, y2, color='blue', label="V2")

ax1.set_xlabel("r")
ax1.set_ylabel("E")
ax1.set_xlim(-3,3)
ax1.set_ylim(0,20)



###### optimize 

import math
from polylib import path_integral
#xA = xB = 0
xA = -2 ; xB = 2

PI1 = path_integral.fixed(v1, v1.mass, xA, xB)
PI2 = path_integral.fixed(v2, v2.mass, xA, xB)

N1 = 10
x1 = -np.cos(np.arange(N1)*math.pi/N1)

from scipy import optimize
tau = 2.0
res = optimize.minimize(lambda x: PI1.S(x,tau), x1, jac=lambda x: PI1.dSdx(x,tau))
x1 = res.x
V1 = [v1.potential(x) for x in x1]
G1 = [v1.gradient(x) for x in x1]
H1 = [v1.hessian(x) for x in x1]
ax1.plot(x1, V1, 'o-')

#PI1.d2Sdx2(x,tau) # 2nd-derivs of beads in instanton

#PI1.S(x,tau) # calculate action at any path x

##### analyze the instanton

"""
from polylib import trajectory

dt = tau/N1 # check that
traj1 = trajectory.Trajectory(x1, V1, G1, H1, dt, v1.mass)
S1 = traj1.S()
dS1dx = traj1.dSdx()
d2S1dx2 = traj1.d2Sdx2()

C1 = - d2S1dx1[0,1]

# imaginary-time van-Vleck propagator from xA to xB in imaginary time tau
K1 = math.sqrt(C/(2*math.pi)) * math.exp(-S1)

"""

plt.show()