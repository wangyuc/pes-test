import numpy as np
from polylib.PES import oneD


custom_pes = oneD.double_well(1, 0.)


a = custom_pes.potential(10)

print(a)

#TODO: plot the potential to see how it looks like
#Hint: use matplotlib and numpy

def f(x):
    """A linear function"""
    return x

a = f(0)
print(a)